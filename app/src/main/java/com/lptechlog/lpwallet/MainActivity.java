package com.lptechlog.lpwallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lptechlog.lpwallet.custom.GenerateQr;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    Button login;
    EditText phonenumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        login=(Button) findViewById(R.id.nextlogin);
        phonenumber=(EditText)findViewById(R.id.phonenumber) ;
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phonenumber.getText().toString().length()!=0) {
                    if (isValidMobile(phonenumber.getText().toString())) {
                        startActivity(new Intent(MainActivity.this, SMSRetrieverActivity.class));
                    }
                }else{
                    Toast.makeText(MainActivity.this, "Enter Mobile number", Toast.LENGTH_SHORT).show();
                }
            }

        });
    }



    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {

            char c = phone.charAt(0);
            String e= String.valueOf(c);
            int a=Integer.parseInt(e);
            boolean isDigit = (a >= 6 && a <= 9);



            if(phone.length() < 10 || phone.length() > 10 ||!isDigit) {
                // if(phone.length() != 10) {
                check = false;
                Toast.makeText(this, "Not a Valid Number", Toast.LENGTH_SHORT).show();
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refer) {
            startActivity(new Intent(MainActivity.this,GenerateQr.class));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
