package com.lptechlog.lpwallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class Payamount extends AppCompatActivity {
    Intent in;
    String name;
    String number;
    TextView tvname,tvnumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payamount);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                onBackPressed();
            }
        });
        tvname=findViewById(R.id.tvname);
        tvnumber=findViewById(R.id.tvnumber);

        in=getIntent();

        try {
            name=in.getStringExtra("name");
        } catch (Exception e) {
            e.printStackTrace();
            name="Dummy Name";
        }
        try {
            number=in.getStringExtra("number");
        } catch (Exception e) {
            e.printStackTrace();
            number="XXXXXXXXXX";
        }

        if(name.equalsIgnoreCase("headingyoyolpwallet")){
            tvname.setText(""+number);

            tvnumber.setVisibility(View.GONE);
        }else{
            tvname.setText(""+name);
            tvnumber.setText(""+number);
            tvnumber.setVisibility(View.VISIBLE);
        }


    }

}
