package com.lptechlog.lpwallet;

import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.lptechlog.lpwallet.custom.ProductItem;
import com.lptechlog.lpwallet.custom.ProductlistAdapter;
import com.lptechlog.lpwallet.custom.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductslistActivity extends AppCompatActivity {
    LinearLayout listviewimglayout,gridviewimglayout;
    GridView gridviewmain;
    ProductlistAdapter adap;
    List<ProductItem> productsList;
    private RecyclerView recyclerView;
    String cata;
    ImageView gridviewimg,listviewimg;
    float finalheight,finalwidth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productslist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        finalheight= height/3;
        finalwidth=width/5;

        listviewimglayout=(LinearLayout)findViewById(R.id.listviewimg);
        gridviewimglayout=(LinearLayout)findViewById(R.id.gridviewimg);
        gridviewimg=(ImageView)findViewById(R.id.gridimg);
        listviewimg=(ImageView)findViewById(R.id.listimg);
        listviewimglayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listviewimg.setImageResource(R.drawable.listblack);
                gridviewimg.setImageResource(R.drawable.gridrey);
                adap=new ProductlistAdapter(ProductslistActivity.this,productsList,Math.round(finalwidth),1);
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(ProductslistActivity.this, 1);
                recyclerView.setLayoutManager(mLayoutManager);

                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adap);
                adap.notifyDataSetChanged();
            }
        });

        gridviewimglayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listviewimg.setImageResource(R.drawable.listgrey);
                gridviewimg.setImageResource(R.drawable.gridblack);
                adap=new ProductlistAdapter(ProductslistActivity.this,productsList,Math.round(finalheight),0);
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(ProductslistActivity.this, 2);
                recyclerView.setLayoutManager(mLayoutManager);

                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adap);
                adap.notifyDataSetChanged();
            }
        });

        gridviewmain=(GridView) findViewById(R.id.gridviewmain);
        productsList=new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        adap=new ProductlistAdapter(ProductslistActivity.this,productsList,Math.round(finalheight),0);

//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//        recyclerView.setLayoutManager(mLayoutManager);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adap);

        cata="productslist";

        new GetContacts(cata).execute();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ProductItem item=productsList.get(position);
                Toast.makeText(getApplicationContext(), item.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }


    private class GetContacts extends AsyncTask<Void, Void, Void> {

        View v;
        String cat1,cat2,cat3,cat4,cat5,cat6;
        public GetContacts(String ss1) {

            cat1=ss1;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Toast.makeText(CategorieslistActivity.this,"Json Data is downloading",Toast.LENGTH_LONG).show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            // Below code from server url


//            HttpHandler sh = new HttpHandler();
//            // Making a request to url and getting response
//            String url = "http://api.androidhive.info/contacts/";
//            String jsonStr = sh.makeServiceCall(url);


            //below code for json stored in resources

            String jsonStr = loadJSONFromAsset();

            Log.e("Fashion", "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray contacts1 = jsonObj.getJSONArray(cat1);

                    // looping through All Contacts

                    for (int i = 0; i < contacts1.length(); i++) {
                        JSONObject c = contacts1.getJSONObject(i);
                        String id = c.getString("id");
                        String name = c.getString("name");
                        String offerrate = c.getString("offerrate");
                        String orginalrate = c.getString("orginalrate");
                        String percentoffer = c.getString("percentoffer");
                        String imageurl = c.getString("imageurl");

                        ProductItem item=new ProductItem(name,offerrate,orginalrate,percentoffer,imageurl);
                        productsList.add(item);


                        // tmp hash map for single contact
//                        HashMap<String, String> contact = new HashMap<>();
//                        // adding each child node to HashMap key => value
//                        contact.put("id", id);
//                        contact.put("name", name);
//                        contact.put("offerrate", "Rs. "+offerrate+" ");
//                        contact.put("orginalrate", " "+orginalrate+" ");
//                        contact.put("percentoffer", " "+percentoffer+"% off ");
//                        contact.put("imageurl", imageurl);
//                        // adding contact to contact list
//                        productsList.add(contact);
                    }


                } catch (final JSONException e) {
                    Log.e("", "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ProductslistActivity.this,
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                Log.e("", "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ProductslistActivity.this,
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
//            ListAdapter adapter = new SimpleAdapter(mcontext, fashionList,
//                    R.layout.fashion_homelist, new String[]{ "name"},
//                    new int[]{R.id.nnn});

            adap.notifyDataSetChanged();

//            HashMap<String, String>currentItem=homepageimglist.get(0);


//            Glide
//                    .with(mcontext)
//                    .load(currentItem.get("imageurl").toString())
//                    .into(homepageimg);


        }
    }



    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("productlist.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
