package com.lptechlog.lpwallet.custom;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lptechlog.lpwallet.R;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySimpleAdapter extends BaseAdapter {
    private Context context; //context
    ArrayList<HashMap<String, String>> fashionList;
    String cat;
    public MySimpleAdapter(Activity context, ArrayList<HashMap<String, String>> items,String ss) {
        this.context = context;
        this.fashionList = items;
        cat=ss;
    }

    @Override
    public int getCount() {
        return fashionList.size();
    }

    @Override
    public Object getItem(int position) {
        return fashionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {


            switch (cat) {
                case "fashionlist":
                    convertView = LayoutInflater.from(context).
                            inflate(R.layout.fashion_homelist, parent, false);
                    break;
                case "mahacashbacklist":
                    convertView = LayoutInflater.from(context).
                            inflate(R.layout.mahacashback_homelist, parent, false);
                    break;

                case "topfeaturedoffers":
                    convertView = LayoutInflater.from(context).
                            inflate(R.layout.fashion_homelist, parent, false);
                    break;
                case "grocerylist":
                    convertView = LayoutInflater.from(context).
                            inflate(R.layout.mahacashback_homelist, parent, false);
                    break;
                case "shopbycategorylist":
                    convertView = LayoutInflater.from(context).
                            inflate(R.layout.mahacashback_homelist, parent, false);
                    break;
                }
                holder = new ViewHolder();
            holder.textname = (TextView)convertView.findViewById(R.id.nnn);
            holder.img = (ImageView) convertView.findViewById(R.id.fashimg);
            holder.textname.setVisibility(View.GONE);
            switch (cat) {
                case "fashionlist":

                    break;
                case "mahacashbacklist":

                    break;

                case "topfeaturedoffers":

                    break;
                case "grocerylist":

                    break;
                case "shopbycategorylist":
                    holder.textname.setVisibility(View.GONE);
                    break;
            }


            convertView.setTag(holder);
        } else {
                holder = (ViewHolder)convertView.getTag();
            }

        // get current item to be displayed

        HashMap<String, String>currentItem=fashionList.get(position);


        // get the TextView for item name and item description


        //sets the text for item name and item description from the current item object
        holder.textname.setText(currentItem.get("name").toString());
        Glide
                .with(context)
                .load(currentItem.get("imageurl").toString())
                .into(holder.img);


        // returns the view for the current row
        return convertView;
    }

    public static class ViewHolder {
        public TextView textname;
        public ImageView img;

    }
}
