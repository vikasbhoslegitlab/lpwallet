package com.lptechlog.lpwallet.custom;

public class OrdersItem {

    private String productorderno,
    productorderdate,
    productimgurl,
    producturl,
    productname,
    productorderprice,
    productstatus,
    productstatuscode,
    producttype,
    producttypecode,
    producttransactiontype;

    public OrdersItem(){


    }

    public OrdersItem(String productorderno,
                      String productorderdate,
                      String productimgurl,
                      String producturl,
                      String productname,
                      String productorderprice,
                      String productstatus,
                      String productstatuscode,
                      String producttype,
                      String producttypecode,
                      String producttransactiontype) {
        this.productorderno = productorderno;
        this.productorderdate = productorderdate;
        this.productimgurl = productimgurl;
        this.producturl = producturl;
        this.productname = productname;
        this.productorderprice = productorderprice;
        this.productstatus = productstatus;
        this.productstatuscode = productstatuscode;
        this.producttype = producttype;
        this.producttypecode = producttypecode;
        this.producttransactiontype = producttransactiontype;
    }

    public String getProductorderno() {
        return productorderno;
    }

    public void setProductorderno(String productorderno) {
        this.productorderno = productorderno;
    }

    public String getProductorderdate() {
        return productorderdate;
    }

    public void setProductorderdate(String productorderdate) {
        this.productorderdate = productorderdate;
    }

    public String getProductimgurl() {
        return productimgurl;
    }

    public void setProductimgurl(String productimgurl) {
        this.productimgurl = productimgurl;
    }

    public String getProducturl() {
        return producturl;
    }

    public void setProducturl(String producturl) {
        this.producturl = producturl;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductorderprice() {
        return productorderprice;
    }

    public void setProductorderprice(String productorderprice) {
        this.productorderprice = productorderprice;
    }

    public String getProductstatus() {
        return productstatus;
    }

    public void setProductstatus(String productstatus) {
        this.productstatus = productstatus;
    }

    public String getProductstatuscode() {
        return productstatuscode;
    }

    public void setProductstatuscode(String productstatuscode) {
        this.productstatuscode = productstatuscode;
    }

    public String getProducttype() {
        return producttype;
    }

    public void setProducttype(String producttype) {
        this.producttype = producttype;
    }

    public String getProducttypecode() {
        return producttypecode;
    }

    public void setProducttypecode(String producttypecode) {
        this.producttypecode = producttypecode;
    }

    public String getProducttransactiontype() {
        return producttransactiontype;
    }

    public void setProducttransactiontype(String producttransactiontype) {
        this.producttransactiontype = producttransactiontype;
    }



}
