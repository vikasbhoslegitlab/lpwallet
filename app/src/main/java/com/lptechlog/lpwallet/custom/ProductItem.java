package com.lptechlog.lpwallet.custom;

public class ProductItem {

    private String title, offerrate, orginalrate,percentoffer,imgurl;

    public ProductItem(){


    }

    public ProductItem(String title, String offerrate, String orginalrate,String percentoffer, String imgurl) {
        this.title = title;
        this.offerrate = offerrate;
        this.orginalrate = orginalrate;
        this.percentoffer = percentoffer;
        this.imgurl = imgurl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getOfferrate() {
        return offerrate;
    }

    public void setOfferrate(String name) {
        this.offerrate = name;
    }

    public String getOrginalrate() {
        return orginalrate;
    }

    public void setOrginalrate(String name) {
        this.orginalrate = name;
    }

    public String getPercentoffer() {
        return percentoffer;
    }

    public void setPercentoffer(String name) {
        this.percentoffer = name;
    }

    public String getimgurl() {
        return imgurl;
    }

    public void setImgurl(String name) {
        this.imgurl = name;
    }

}
