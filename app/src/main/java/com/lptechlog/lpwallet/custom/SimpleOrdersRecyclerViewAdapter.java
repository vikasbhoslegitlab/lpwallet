package com.lptechlog.lpwallet.custom;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lptechlog.lpwallet.R;

import java.nio.charset.IllegalCharsetNameException;
import java.util.ArrayList;
import java.util.List;

public class SimpleOrdersRecyclerViewAdapter extends RecyclerView.Adapter<SimpleOrdersRecyclerViewAdapter.MyViewHolder>
        implements Filterable {
    private List<OrdersItem> productList;
    private List<OrdersItem> filteredproductList;
    int hta;
    Activity context;
    int type;

    private ItemFilter mFilter = new ItemFilter();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView productorderno,
                productorderdate,
                producturl,
                productname,
                productorderprice,
                productstatus,
                productstatuscode,
                producttype,
                producttypecode,
                producttransactiontype;
        public ImageView productimgurl;


        public MyViewHolder(View view) {
            super(view);
            productorderno = (TextView) view.findViewById(R.id.productorderno);
            productorderdate = (TextView) view.findViewById(R.id.productorderdate);
            productname = (TextView) view.findViewById(R.id.productname);
            producturl= (TextView) view.findViewById(R.id.producturl);
            productorderprice= (TextView) view.findViewById(R.id.productprice);
            productstatus= (TextView) view.findViewById(R.id.productstatus);
            productstatuscode= (TextView) view.findViewById(R.id.productstatuscode);
            producttype= (TextView) view.findViewById(R.id.producttype);
            producttypecode= (TextView) view.findViewById(R.id.producttypecode);
            producttransactiontype= (TextView) view.findViewById(R.id.producttransactiontype);
            productimgurl=(ImageView)view.findViewById(R.id.productimage);

        }
    }


    public SimpleOrdersRecyclerViewAdapter(Activity mcon,List<OrdersItem> productList,int hh,int type) {
        this.context=mcon;
        this.productList = productList;
        this.hta=hh;
        this.type=type;
        this.filteredproductList=productList;
    }

    @Override
    public SimpleOrdersRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = null;

        switch (type){

            case 0: itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.myordersitem_listview, parent, false);
                break;
            case 1: itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.myopassbookitem_listview, parent, false);
                break;

        }

        return new SimpleOrdersRecyclerViewAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        OrdersItem item=filteredproductList.get(position);

        holder.productname.setText(item.getProductname());
        holder.productorderdate.setText(item.getProductorderdate());
        holder.productorderno.setText("Order No "+item.getProductorderno());
        holder.productorderprice.setText("\u20B9 "+item.getProductorderprice());
        holder.productstatus.setText(item.getProductstatus());
        holder.productstatuscode.setText(item.getProductstatuscode());
        holder.producttype.setText(item.getProducttype());
        holder.producttypecode.setText(item.getProducttypecode());
        holder.producttransactiontype.setText(item.getProducttransactiontype());
        holder.producturl.setText(item.getProducturl());


        int statuscolor=Integer.parseInt(item.getProductstatuscode());

        switch (statuscolor){
            case 1:
                holder.productstatus.setTextColor(context.getResources().getColor(R.color.success));
                break;
            case 2:
                holder.productstatus.setTextColor(context.getResources().getColor(R.color.inprocess));
                break;
            case 3:
                holder.productstatus.setTextColor(context.getResources().getColor(R.color.failed));
                break;

        }


        switch (type){

            case 0:
                holder.productimgurl.getLayoutParams().width=hta;
                holder.productimgurl.getLayoutParams().height=hta;
                break;
            case 1:

                holder.productimgurl.getLayoutParams().width=hta;
                holder.productimgurl.getLayoutParams().height=hta;
                int sta_code=Integer.parseInt(item.getProducttypecode());

                switch (sta_code){

                    case 1:

                        holder.productorderprice.setText("- \u20B9 "+item.getProductorderprice());
                        holder.productorderprice.setTextColor(context.getResources().getColor(R.color.textcolorfadeblack));
                        break;
                    case 2:
                        holder.productorderprice.setText("+ \u20B9 "+item.getProductorderprice());
                        holder.productorderprice.setTextColor(context.getResources().getColor(R.color.success));
                        break;
                    case 3:
                        holder.productorderprice.setText("+ \u20B9 "+item.getProductorderprice());
                        holder.productorderprice.setTextColor(context.getResources().getColor(R.color.success));
                        break;

                }



                break;

        }

//        Glide
//                .with(context)
//                .load(item.getimgurl())
//                .into(holder.img);

    }

    @Override
    public int getItemCount() {
        return filteredproductList.size();
    }


    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<OrdersItem> list = productList;

            int count = list.size();
            final List<OrdersItem> nlist = new ArrayList<OrdersItem>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {

                    filterableString = list.get(i).getProducttypecode().toString();
                    if (filterableString.toLowerCase().contains(filterString)) {

                        String productorderno= list.get(i).getProductorderno();
                        String productorderdate= list.get(i).getProductorderdate();
                        String productimgurl= list.get(i).getProductimgurl();
                        String producturl= list.get(i).getProducturl();
                        String productname= list.get(i).getProductname();
                        String productorderprice= list.get(i).getProductorderprice();
                        String productstatus= list.get(i).getProductstatus();
                        String productstatuscode=list.get(i).getProductstatuscode();
                        String producttype= list.get(i).getProducttype();
                        String producttypecode= list.get(i).getProducttypecode();
                        String producttransactiontype= list.get(i).getProducttransactiontype();



                        OrdersItem itemb=new OrdersItem(productorderno,productorderdate,productimgurl,producturl,
                                productname,productorderprice,productstatus,productstatuscode,producttype,
                                producttypecode,producttransactiontype);

                        nlist.add(itemb);
                    }

            }


            results.values = nlist;
            results.count = nlist.size();

            return results;
        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredproductList = ( ArrayList<OrdersItem>) results.values;
            notifyDataSetChanged();
        }

    }

}
