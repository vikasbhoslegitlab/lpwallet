package com.lptechlog.lpwallet.custom;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.lptechlog.lpwallet.R;

public class GenerateQr extends AppCompatActivity {
    EditText et_id;
    Button generate;
    ImageView ivqrcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_qr);

        et_id=findViewById(R.id.et_id);
        generate=findViewById(R.id.generate);
        ivqrcode=findViewById(R.id.ivqrcode);

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id=et_id.getText().toString();
                WindowManager windowManager= (WindowManager) getSystemService(WINDOW_SERVICE);
                Display display=windowManager.getDefaultDisplay();
                Point point=new Point();
                display.getSize(point);
                int x=point.x;
                int y=point.y;
                int icon=x<y?x:y;
                icon=icon*3/4;
                QRCodeEncoder qrCodeEncoder=new QRCodeEncoder(id,null,Contents.Type.TEXT,BarcodeFormat.QR_CODE.toString(),icon);
                try {
                    Bitmap bitmap=qrCodeEncoder.encodeAsBitmap();

                    ivqrcode.setImageBitmap(bitmap);
                } catch (WriterException e) {
                    e.printStackTrace();
                }

            }
        });

    }
}
