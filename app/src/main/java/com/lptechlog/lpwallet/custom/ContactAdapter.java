package com.lptechlog.lpwallet.custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.lptechlog.lpwallet.Payamount;
import com.lptechlog.lpwallet.R;

import java.util.ArrayList;

public class ContactAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private ArrayList<ContactModel> contactModelArrayList;
    private ArrayList<ContactModel> filterModelArrayList;
    private ItemFilter mFilter = new ItemFilter();

    public ContactAdapter(Context context, ArrayList<ContactModel> contactModelArrayList) {

        this.context = context;
        this.contactModelArrayList = contactModelArrayList;
        this.filterModelArrayList=contactModelArrayList;
    }



    @Override
    public int getCount() {

        int count=0;
        try {
            count=filterModelArrayList.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        return filterModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, null, true);

            holder.tvname = (TextView) convertView.findViewById(R.id.name);
            holder.tvnumber = (TextView) convertView.findViewById(R.id.noofproducts);

            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }

        holder.tvname.setText(filterModelArrayList.get(position).getName());
        holder.tvnumber.setText(filterModelArrayList.get(position).getNumber());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, ""+position, Toast.LENGTH_SHORT).show();
                Intent i= new Intent(context,Payamount.class);
                i.putExtra("name",""+filterModelArrayList.get(position).getName());
                i.putExtra("number",""+filterModelArrayList.get(position).getNumber());
                context.startActivity(i);
            }
        });

        return convertView;
    }

    private class ViewHolder {

        protected TextView tvname, tvnumber;

    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<ContactModel> list = contactModelArrayList;

            int count = list.size();
            final ArrayList<ContactModel> nlist = new ArrayList<ContactModel>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {


                if (isNumber(filterString)) {
                    filterableString = list.get(i).getNumber().toString();
                    if (filterableString.toLowerCase().contains(filterString)) {

                        String name = list.get(i).getName().toString();
                        String phoneNumber = list.get(i).getNumber().toString();

                        ContactModel contactModel = new ContactModel();
                        contactModel.setName(name);
                        contactModel.setNumber(phoneNumber);
                        nlist.add(contactModel);
                     }
                 }
                  else{
                    filterableString = list.get(i).getName().toString();
                    if (filterableString.toLowerCase().contains(filterString)) {

                        String name = list.get(i).getName().toString();
                        String phoneNumber = list.get(i).getNumber().toString();

                        ContactModel contactModel = new ContactModel();
                        contactModel.setName(name);
                        contactModel.setNumber(phoneNumber);
                        nlist.add(contactModel);
                    }

                    }
                }


            results.values = nlist;
            results.count = nlist.size();

            return results;
        }
        public boolean isNumber(String s)
        {
            for (int i = 0; i < s.length(); i++)
                if (Character.isDigit(s.charAt(i))
                        == false)
                    return false;

            return true;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filterModelArrayList = ( ArrayList<ContactModel>) results.values;
            notifyDataSetChanged();
        }

    }
}