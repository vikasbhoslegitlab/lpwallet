package com.lptechlog.lpwallet.custom;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lptechlog.lpwallet.R;

import java.util.List;

public class ProductlistAdapter extends RecyclerView.Adapter<ProductlistAdapter.MyViewHolder> {
    private List<ProductItem> productList;
    int hta;
    Activity context;
    int type;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, offerate, orginalrate,percentoffer;
        public ImageView img;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            offerate = (TextView) view.findViewById(R.id.offerrate);
            orginalrate = (TextView) view.findViewById(R.id.orginalrate);
            percentoffer= (TextView) view.findViewById(R.id.percentoffer);
            img=(ImageView)view.findViewById(R.id.fashimg);

        }
    }


    public ProductlistAdapter(Activity mcon,List<ProductItem> productList,int hh,int type) {
        this.context=mcon;
        this.productList = productList;
        this.hta=hh;
        this.type=type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = null;

        switch (type){

            case 0: itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.productlistitem_gridview, parent, false);
                    break;
            case 1: itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.productlistitem_listview, parent, false);
                    break;

        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        ProductItem item=productList.get(position);
        holder.title.setText(item.getTitle());
        holder.offerate.setText("Rs. "+item.getOfferrate()+"  ");
        holder.orginalrate.setText(" "+item.getOrginalrate()+" ");
        holder.percentoffer.setText(" "+item.getPercentoffer()+"% off");

        switch (type){

            case 0:
                holder.img.getLayoutParams().height=hta;
                break;
            case 1:

                holder.img.getLayoutParams().width=hta;
                holder.img.getLayoutParams().height=hta;
                break;

        }

        Glide
                .with(context)
                .load(item.getimgurl())
                .into(holder.img);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
