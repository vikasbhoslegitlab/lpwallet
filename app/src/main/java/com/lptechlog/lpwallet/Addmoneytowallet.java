package com.lptechlog.lpwallet;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Addmoneytowallet extends AppCompatActivity {

    LinearLayout netbanking;
    BottomSheetBehavior sheetBehavior;
    LinearLayout layoutBottomSheet;
    BottomSheetDialog mdialog;
    TextView cancel;
    TextView payableamount;
    ListView listView;
    String amnttodisplay;
    String[] banksArray = {"Axis Bank","Allahabad Bank","Andhra Bank","Bank of Baroda","Bank of India",
            "Bank of Maharashtra","Canara Bank","City Union Bank","Central Bank Of India","Corporation Bank",
            "Dena Bank","Dhanalakshmi Bank","Federal Bank","IDBI Bank","Indian Bank",
            "Indusland Bank","Karur Vysya Bank","Kotak Bank","Syndicate Bank","UCO Bank",};
    Intent in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addmoneytowallet);
        in=getIntent();
        amnttodisplay= String.valueOf(in.getIntExtra("amount",0));
        setids();
        setdialogids();

        payableamount.setText("Rs. "+amnttodisplay);
        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.onetextitem, banksArray);
        listView.setAdapter(adapter);
       setlistners();

    }


    private void setids() {

        mdialog = new BottomSheetDialog(Addmoneytowallet.this);
        mdialog.setContentView(R.layout.netbankingpopup);
        netbanking=findViewById(R.id.netbanking);
        payableamount=findViewById(R.id.payableamount);
    }
    private void setdialogids() {
        listView = (ListView) mdialog.findViewById(R.id.listview);
        cancel=mdialog.findViewById(R.id.cancel);
    }

    private void setlistners() {

        netbanking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdialog.show();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdialog.dismiss();
            }
        });
    }


}
