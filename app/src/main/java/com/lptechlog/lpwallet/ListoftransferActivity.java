package com.lptechlog.lpwallet;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.lptechlog.lpwallet.R;
import com.lptechlog.lpwallet.fragments.FiveFragment;
import com.lptechlog.lpwallet.fragments.FourFragment;
import com.lptechlog.lpwallet.fragments.IMPSFragment;
import com.lptechlog.lpwallet.fragments.TwoFragment;
import com.lptechlog.lpwallet.fragments.UPIFragment;
import com.lptechlog.lpwallet.fragments.ViewPagerAdapter;
import com.lptechlog.lpwallet.fragments.WalletFragment;

import java.util.ArrayList;
import java.util.List;

public class ListoftransferActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public  static final int RequestPermissionCode  = 1 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listofproducts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        EnableRuntimePermission();

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }
    private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.ic_tab_favourite,
                R.drawable.ic_tab_call,
                R.drawable.ic_tab_call,
        };

//        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
//        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
//        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new UPIFragment(ListoftransferActivity.this), "UPI");
        adapter.addFrag(new WalletFragment(ListoftransferActivity.this), "WALLET");
        adapter.addFrag(new IMPSFragment(ListoftransferActivity.this), "IMPS");
        viewPager.setAdapter(adapter);
    }



    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(ListoftransferActivity.this,
                Manifest.permission.READ_CONTACTS))
        {

            Toast.makeText(ListoftransferActivity.this,"CONTACTS permission allows us to Access CONTACTS app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(ListoftransferActivity.this,new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(ListoftransferActivity.this,"Permission Granted, Now your application can access CONTACTS.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(ListoftransferActivity.this,"Permission Canceled, Now your application cannot access CONTACTS.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }

}
