package com.lptechlog.lpwallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bigbangbutton.editcodeview.EditCodeListener;
import com.bigbangbutton.editcodeview.EditCodeView;
import com.bigbangbutton.editcodeview.EditCodeWatcher;

public class SMSRetrieverActivity extends AppCompatActivity {
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsretriever);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Back");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                onBackPressed();
            }
        });


        EditCodeView editCodeView = (EditCodeView) findViewById(R.id.phonecode);
        editCodeView.setEditCodeListener(new EditCodeListener() {
            @Override
            public void onCodeReady(String code) {

                //Toast.makeText(SMSRetrieverActivity.this, ""+code, Toast.LENGTH_SHORT).show();

            }
        });

        editCodeView.setEditCodeWatcher(new EditCodeWatcher() {
            @Override
            public void onCodeChanged(String code) {
                Log.e("CodeWatcher", " changed : " + code);

                if(Integer.parseInt(code)==1234){
                    startActivity(new Intent(SMSRetrieverActivity.this,HomePage.class));
                }
                else{
                    if(code.length()==4)
                    Toast.makeText(SMSRetrieverActivity.this, "Please enter correct id", Toast.LENGTH_SHORT).show();
                }

            }
        });

        editCodeView.requestFocus();




        submit=(Button)findViewById(R.id.submitverified);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SMSRetrieverActivity.this,HomePage.class));


            }
        });
    }

}
