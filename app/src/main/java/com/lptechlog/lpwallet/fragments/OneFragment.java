package com.lptechlog.lpwallet.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.lptechlog.lpwallet.AceptPaymentActivity;
import com.lptechlog.lpwallet.Addmoneytowallet;
import com.lptechlog.lpwallet.BillPayments;
import com.lptechlog.lpwallet.CategorieslistActivity;
import com.lptechlog.lpwallet.HomePage;
import com.lptechlog.lpwallet.ListoftransferActivity;
import com.lptechlog.lpwallet.QrActivity;
import com.lptechlog.lpwallet.R;
import com.lptechlog.lpwallet.custom.CustomScannerActivity;
import com.lptechlog.lpwallet.custom.ExpandableHeightGridView;
import com.lptechlog.lpwallet.custom.MySimpleAdapter;
import com.lptechlog.lpwallet.custom.PassbookDetails;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;


public class OneFragment extends Fragment{
    SliderLayout sliderLayout;
    Button viewallcat;
    Activity mcontext;
    BottomSheetDialog mdialog;
    ArrayList<HashMap<String, String>> fashionList,mahacashbacklist,topfeaturedlist,grocerylist,shopbycategorylist,homepageimglist;
    private String TAG = HomePage.class.getSimpleName();
    String cata,catb,catc,catd,cate,catf;
    MySimpleAdapter adap1,adap2,adap3,adap4,adap5;
    ImageView homepageimg;
    LinearLayout t1layout,t2layout,t3layout,t4layout,t5layout;
    LinearLayout b1layout,b2layout,b3layout,b4layout,b5layout,b6layout,b7layout,b8layout,b9layout,b10layout;
    Button cont;
    TextView rp1,rp2,rp3,cancel;
    EditText moneyvalue;
    public OneFragment() {
        // Required empty public constructor
    }
    @SuppressLint("ValidFragment")
    public OneFragment(Activity con) {
        // Required empty public constructor
        mcontext=con;
        cata="fashionlist";
        catb="mahacashbacklist";
        catc="topfeaturedoffers";
        catd="grocerylist";
        cate="shopbycategorylist";
        catf="homepageimageurl";
//        mdialog=new Dialog(con,R.style.MyDialogTheme);
//        mdialog.setContentView(R.layout.addmoneypopup);

        mdialog = new BottomSheetDialog(mcontext);
        mdialog.setContentView(R.layout.addmoneypopup);
        cont=mdialog.findViewById(R.id.cont);
        rp1=mdialog.findViewById(R.id.rp1);
        rp2=mdialog.findViewById(R.id.rp2);
        rp3=mdialog.findViewById(R.id.rp3);
        cancel=mdialog.findViewById(R.id.cancel);
        moneyvalue=mdialog.findViewById(R.id.addmoneyedittext);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v=inflater.inflate(R.layout.fragment_one, container, false);

        DisplayMetrics metrics = new DisplayMetrics();
        mcontext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        float finalheight= height/3;

        sliderLayout=(SliderLayout)v.findViewById(R.id.imageSlider);
        viewallcat=(Button)v.findViewById(R.id.viewallcategories);
        homepageimg=(ImageView)v.findViewById(R.id.homepageimg);
        t1layout=(LinearLayout)v.findViewById(R.id.t1layout);
        t2layout=(LinearLayout)v.findViewById(R.id.t2layout);
        t3layout=(LinearLayout)v.findViewById(R.id.t3layout);
        t4layout=(LinearLayout)v.findViewById(R.id.t4layout);
        t5layout=(LinearLayout)v.findViewById(R.id.t5layout);

        b1layout=(LinearLayout)v.findViewById(R.id.b1layout);
        b2layout=(LinearLayout)v.findViewById(R.id.b2layout);
        b3layout=(LinearLayout)v.findViewById(R.id.b3layout);
        b4layout=(LinearLayout)v.findViewById(R.id.b4layout);
        b5layout=(LinearLayout)v.findViewById(R.id.b5layout);
        b6layout=(LinearLayout)v.findViewById(R.id.b6layout);
        b7layout=(LinearLayout)v.findViewById(R.id.b7layout);
        b8layout=(LinearLayout)v.findViewById(R.id.b8layout);
        b9layout=(LinearLayout)v.findViewById(R.id.b9layout);
        b10layout=(LinearLayout)v.findViewById(R.id.b10layout);



        sliderLayout.getLayoutParams().height=(int)Math.round(finalheight);
        homepageimg.getLayoutParams().height=(int)Math.round(finalheight);

        sliderLayout.setIndicatorAnimation(SliderLayout.Animations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(4); //set scroll delay in seconds :

        setSliderViews();
        sethomepage(v);

        gethomepagedata(v);

        viewallcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mcontext,CategorieslistActivity.class));
            }
        });
        setlistners();
        return v;
    }

    private void setlistners() {

        t1layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mcontext,QrActivity.class));
            }
        });

        t2layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mcontext,ListoftransferActivity.class));
            }
        });
        t3layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mcontext,PassbookDetails.class));
            }
        });
        t4layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  startActivity(new Intent(mcontext,QrActivity.class));
                //mdialog.show();

                mdialog.show();


            }
        });
        t5layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mcontext,AceptPaymentActivity.class));
            }
        });

        b1layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",0);
                startActivity(i);
            }
        });
        b2layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",1);
                startActivity(i);
            }
        });
        b3layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",2);
                startActivity(i);
            }
        });
        b4layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",3);
                startActivity(i);
            }
        });
        b5layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",4);
                startActivity(i);
            }
        });
        b6layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",5);
                startActivity(i);
            }
        });
        b7layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",6);
                startActivity(i);
            }
        });
        b8layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",7);
                startActivity(i);
            }
        });
        b9layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",8);
                startActivity(i);
            }
        });
        b10layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext,BillPayments.class);
                i.putExtra("key",9);
                startActivity(i);
            }
        });



        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mvalue=moneyvalue.getText().toString();
                int moneyvalueint= 0;
                try {
                    moneyvalueint = Integer.parseInt(mvalue);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                if(mvalue.length()!=0 && !mvalue.equalsIgnoreCase("0")) {
                    Toast.makeText(mcontext, "Adding Rs." + moneyvalue.getText()+" /- to lp wallet", Toast.LENGTH_SHORT).show();
                    Intent i=new Intent(mcontext,Addmoneytowallet.class);
                    i.putExtra("amount",moneyvalueint);
                    startActivity(i);
                    mdialog.dismiss();
                }else{
                    Toast.makeText(mcontext, "Please enter correct amount", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moneyvalue.setText("1000");
            }
        });
        rp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moneyvalue.setText("500");
            }
        });
        rp3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moneyvalue.setText("100");
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdialog.dismiss();
            }
        });


    }

    private void sethomepage(View v) {

        fashionList = new ArrayList<>();
        mahacashbacklist = new ArrayList<>();
        topfeaturedlist = new ArrayList<>();
        grocerylist = new ArrayList<>();
        shopbycategorylist= new ArrayList<>();
        homepageimglist= new ArrayList<>();
        adap1=  new MySimpleAdapter(mcontext,fashionList,cata);
        adap2=  new MySimpleAdapter(mcontext,mahacashbacklist,catb);
        adap3=  new MySimpleAdapter(mcontext,topfeaturedlist,catc);
        adap4=  new MySimpleAdapter(mcontext,grocerylist,catd);
        adap5=  new MySimpleAdapter(mcontext,grocerylist,cate);
        TwoWayView fashionlvTest = (TwoWayView) v.findViewById(R.id.lvItems);
        TwoWayView mahacashbacklvTest = (TwoWayView) v.findViewById(R.id.cashbacklvItems);
        TwoWayView topfeaturedlvTest = (TwoWayView) v.findViewById(R.id.topofferslvItems);
        TwoWayView grocerylvTest = (TwoWayView) v.findViewById(R.id.grocerylvItems);
        ExpandableHeightGridView shopbycategorylvTest=(ExpandableHeightGridView) v.findViewById(R.id.shopbycategorylvItems);
        fashionlvTest.setAdapter(adap1);
        mahacashbacklvTest.setAdapter(adap2);
        topfeaturedlvTest.setAdapter(adap3);
        grocerylvTest.setAdapter(adap4);
        shopbycategorylvTest.setAdapter(adap5);
        shopbycategorylvTest.setExpanded(true);
        shopbycategorylvTest.setFocusable(false);


    }

    private void gethomepagedata(View v) {

        new GetContacts(v,"fashionlist","mahacashbacklist","topfeaturedoffers","grocerylist","shopbycategorylist","homepageimageurl").execute();
    }

//    private void setfashionlist(View v) {

//        new GetContacts(v,"fashionlist").execute();
//    }
//    private void setcashbacklist(View v) {
//        new GetContacts(v,"mahacashbacklist").execute();
//    }
//    private void settopfeaturedoffers(View v) {
//        new GetContacts(v,"topfeaturedoffers").execute();
//    }
//    private void setgrocery(View v) {
//        new GetContacts(v,"grocerylist").execute();
//    }


    private void setSliderViews() {

        for (int i = 0; i <= 3; i++) {

            SliderView sliderView = new SliderView(mcontext);

            switch (i) {
                case 0:
                    sliderView.setImageUrl("http://lptechlog.com/demos/lpwallet/images/1.jpg");
                    break;
                case 1:
                    sliderView.setImageUrl("http://lptechlog.com/demos/lpwallet/images/2.jpg");
                    break;
                case 2:
                    sliderView.setImageUrl("http://lptechlog.com/demos/lpwallet/images/3.jpg");
                    break;
                case 3:
                    sliderView.setImageUrl("https://img.freepik.com/free-photo/black-friday-composition-with-four-bags-and-cart_23-2147709334.jpg");
                    break;
            }

            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
            sliderView.setDescription("Welcome to LP Wallet");
            final int finalI = i;
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    Toast.makeText(mcontext, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
                }
            });

            //at last add this view in your layout :
            sliderLayout.addSliderView(sliderView);
        }
    }

    private class GetContacts extends AsyncTask<Void, Void, Void> {

        View v;
        String cat1,cat2,cat3,cat4,cat5,cat6;
        public GetContacts(View vv,String ss1,String ss2,String ss3,String ss4,String ss5,String ss6) {
            v=vv;
            cat1=ss1;
            cat2=ss2;
            cat3=ss3;
            cat4=ss4;
            cat5=ss5;
            cat6=ss6;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Toast.makeText(CategorieslistActivity.this,"Json Data is downloading",Toast.LENGTH_LONG).show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            // Below code from server url


//            HttpHandler sh = new HttpHandler();
//            // Making a request to url and getting response
//            String url = "http://api.androidhive.info/contacts/";
//            String jsonStr = sh.makeServiceCall(url);


            //below code for json stored in resources

            String jsonStr = loadJSONFromAsset();

            Log.e("Fashion", "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray contacts1 = jsonObj.getJSONArray(cat1);
                    JSONArray contacts2 = jsonObj.getJSONArray(cat2);
                    JSONArray contacts3 = jsonObj.getJSONArray(cat3);
                    JSONArray contacts4 = jsonObj.getJSONArray(cat4);
                    JSONArray contacts5 = jsonObj.getJSONArray(cat5);
                    JSONArray contacts6 = jsonObj.getJSONArray(cat6);

                    // looping through All Contacts

                    for (int i = 0; i < contacts1.length(); i++) {
                        JSONObject c = contacts1.getJSONObject(i);
                        String id = c.getString("id");
                        String name = c.getString("name");
                        String noofproducts = c.getString("noofproducts");
                        String imageurl = c.getString("imageurl");
                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();
                        // adding each child node to HashMap key => value
                        contact.put("id", id);
                        contact.put("name", name);
                        contact.put("noofproducts", noofproducts+" products");
                        contact.put("imageurl", imageurl);
                        // adding contact to contact list
                        fashionList.add(contact);
                    }

                    for (int i = 0; i < contacts2.length(); i++) {
                        JSONObject c = contacts2.getJSONObject(i);
                        String id = c.getString("id");
                        String name = c.getString("name");
                        String noofproducts = c.getString("noofproducts");
                        String imageurl = c.getString("imageurl");
                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();
                        // adding each child node to HashMap key => value
                        contact.put("id", id);
                        contact.put("name", name);
                        contact.put("noofproducts", noofproducts+" products");
                        contact.put("imageurl", imageurl);
                        // adding contact to contact list
                        mahacashbacklist.add(contact);
                    }
                    for (int i = 0; i < contacts3.length(); i++) {
                        JSONObject c = contacts3.getJSONObject(i);
                        String id = c.getString("id");
                        String name = c.getString("name");
                        String noofproducts = c.getString("noofproducts");
                        String imageurl = c.getString("imageurl");
                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();
                        // adding each child node to HashMap key => value
                        contact.put("id", id);
                        contact.put("name", name);
                        contact.put("noofproducts", noofproducts+" products");
                        contact.put("imageurl", imageurl);
                        // adding contact to contact list
                        topfeaturedlist.add(contact);
                    }

                    for (int i = 0; i < contacts4.length(); i++) {
                        JSONObject c = contacts4.getJSONObject(i);
                        String id = c.getString("id");
                        String name = c.getString("name");
                        String noofproducts = c.getString("noofproducts");
                        String imageurl = c.getString("imageurl");
                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();
                        // adding each child node to HashMap key => value
                        contact.put("id", id);
                        contact.put("name", name);
                        contact.put("noofproducts", noofproducts+" products");
                        contact.put("imageurl", imageurl);
                        // adding contact to contact list
                        grocerylist.add(contact);
                    }

                    for (int i = 0; i < contacts5.length(); i++) {
                        JSONObject c = contacts5.getJSONObject(i);
                        String id = c.getString("id");
                        String name = c.getString("name");
                        String noofproducts = c.getString("noofproducts");
                        String imageurl = c.getString("imageurl");
                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();
                        // adding each child node to HashMap key => value
                        contact.put("id", id);
                        contact.put("name", name);
                        contact.put("noofproducts", noofproducts+" products");
                        contact.put("imageurl", imageurl);
                        // adding contact to contact list
                        shopbycategorylist.add(contact);
                    }
                    for (int i = 0; i < contacts6.length(); i++) {
                        JSONObject c = contacts6.getJSONObject(i);
                        String imageurl = c.getString("imageurl");
                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();
                        // adding each child node to HashMap key => value
                        contact.put("imageurl", imageurl);
                        // adding contact to contact list
                        homepageimglist.add(contact);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    mcontext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mcontext,
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                Log.e(TAG, "Couldn't get json from server.");
                mcontext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mcontext,
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
//            ListAdapter adapter = new SimpleAdapter(mcontext, fashionList,
//                    R.layout.fashion_homelist, new String[]{ "name"},
//                    new int[]{R.id.nnn});

            adap1.notifyDataSetChanged();
            adap2.notifyDataSetChanged();
            adap3.notifyDataSetChanged();
            adap4.notifyDataSetChanged();
            adap5.notifyDataSetChanged();

            HashMap<String, String>currentItem=homepageimglist.get(0);


            Glide
                    .with(mcontext)
                    .load(currentItem.get("imageurl").toString())
                    .into(homepageimg);


        }
    }



    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = mcontext.getAssets().open("homepage.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
