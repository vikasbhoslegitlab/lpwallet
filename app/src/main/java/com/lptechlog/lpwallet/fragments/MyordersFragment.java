package com.lptechlog.lpwallet.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lptechlog.lpwallet.R;
import com.lptechlog.lpwallet.custom.OrdersItem;
import com.lptechlog.lpwallet.custom.SimpleOrdersRecyclerViewAdapter;

import java.util.List;


@SuppressLint("ValidFragment")
public class MyordersFragment extends Fragment{
    RecyclerView recyclerView;
    Activity mcontext;
    float finalheight,finalwidth;
    BottomSheetDialog mdialog;
    SimpleOrdersRecyclerViewAdapter adapter;
    LinearLayout filter;
    List<OrdersItem> myordersList;
    RadioGroup radioGroup;
    TextView filtertext;
    public MyordersFragment(Activity con, List<OrdersItem> ss) {
        // Required empty public constructor
        mcontext=con;
        myordersList=ss;
        DisplayMetrics metrics = new DisplayMetrics();
        con.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        finalheight= height/3;
        finalwidth=width/7;
        mdialog = new BottomSheetDialog(mcontext);
        mdialog.setContentView(R.layout.bottomsheet_myordersfilter);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_myorders, container, false);
        // Inflate the layout for this fragment
        filter=view.findViewById(R.id.filter);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdialog.show();
            }
        });
        radioGroup=(RadioGroup)mdialog.findViewById(R.id.radioGroup);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        filtertext=view.findViewById(R.id.filtertext);
        setupRecyclerView(recyclerView);

        return view;
    }

    private void setlistners(final SimpleOrdersRecyclerViewAdapter ss) {

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected

                int index = group.indexOfChild(mdialog.findViewById(checkedId));
                //Toast.makeText(mcontext, ""+index, Toast.LENGTH_SHORT).show();
               // ss.getFilter().filter(""+index);
                RadioButton ssa=mdialog.findViewById(checkedId);
                filtertext.setText(ssa.getText());

                if(index==0){
                    ss.getFilter().filter("");
                }
                 else{
                    ss.getFilter().filter(""+index);
                }

                switch (index){
                    case 0:
                        //filtertext.setText(mdialog.findViewById(checkedId).gettext());
                        break;
                    case 1:
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                }


                if(mdialog.isShowing()){
                    mdialog.dismiss();
                }
            }
        });
    }

    private void setupRecyclerView(RecyclerView recyclerView){
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));

        adapter=new SimpleOrdersRecyclerViewAdapter(mcontext,
                myordersList,Math.round(finalwidth),0);
        recyclerView.setAdapter(adapter);
        setlistners(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Toast.makeText(mcontext, "a", Toast.LENGTH_SHORT).show();
    }
}
