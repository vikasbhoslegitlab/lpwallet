package com.lptechlog.lpwallet.fragments;
import com.lptechlog.lpwallet.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lptechlog.lpwallet.custom.OrdersItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@SuppressLint("ValidFragment")
public class TwoFragment extends Fragment{

    FragmentActivity mcontext;

    public TwoFragment(Activity con) {
        // Required empty public constructor
        mcontext=(FragmentActivity) con;
    }


    TextView refer;
    TabLayout tabLayout;
    AppBarLayout appbarlayout;
    ViewPager viewpager;
    List<OrdersItem> myordersList,mypassbooklist;
    ViewPagerAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view=inflater.inflate(R.layout.fragment_two, container, false);

    //   refer=(TextView) view.findViewById(R.id.myreferend);
        myordersList=new ArrayList<>();
        mypassbooklist=new ArrayList<>();
       tabLayout=view.findViewById(R.id.tablayout_profile);
       viewpager=view.findViewById(R.id.viewpager_profile);

        setupViewPager(viewpager);
        tabLayout.setupWithViewPager(viewpager);


        new GetContacts(view,"myorders","mypassbook").execute();

//        refer.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View v) {
//               startActivity(new Intent(v.getContext(),BonusActivity.class));
//           }
//       });
        return view;
    }

    private void setupViewPager(ViewPager viewpager) {

        adapter = new ViewPagerAdapter(mcontext.getSupportFragmentManager());
        adapter.addFrag(new MyordersFragment(mcontext,myordersList),"Orders");
        adapter.addFrag(new MypassbookFragment(mcontext,mypassbooklist), "Passbook");
        adapter.addFrag(new MyReferendFragment(), "Referend");
        adapter.addFrag(new MypaymentsFragment(), "Payments");
        //viewpager.setOffscreenPageLimit(0);
        viewpager.setAdapter(adapter);
    }


    private class GetContacts extends AsyncTask<Void, Void, Void> {

        View v;
        String cat1,cat2;
        public GetContacts(View vv,String ss1,String ss2) {
            v=vv;
            cat1=ss1;
            cat2=ss2;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Toast.makeText(CategorieslistActivity.this,"Json Data is downloading",Toast.LENGTH_LONG).show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            // Below code from server url

//            HttpHandler sh = new HttpHandler();
//            // Making a request to url and getting response
//            String url = "http://api.androidhive.info/contacts/";
//            String jsonStr = sh.makeServiceCall(url);


            //below code for json stored in resources

            String jsonStr = loadJSONFromAsset();

            Log.e("Fashion", "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray contacts1 = jsonObj.getJSONArray(cat1);
                    JSONArray contacts2 = jsonObj.getJSONArray(cat2);

                    // looping through All Contacts

                    for (int i = 0; i < contacts1.length(); i++) {
                        JSONObject c = contacts1.getJSONObject(i);
                        String id = c.getString("id");
                        String productorderno= c.getString("productorderno");
                        String productorderdate= c.getString("productorderdate");
                        String productimgurl= c.getString("producturl");
                        String producturl= c.getString("producturl");
                        String productname= c.getString("productname");
                        String productorderprice= c.getString("productprice");
                        String productstatus= c.getString("productstatus");
                        String productstatuscode= c.getString("productstatuscode");
                        String producttype= c.getString("producttype");
                        String producttypecode= c.getString("producttypecode");
                        String producttransactiontype= c.getString("producttransactiontype");



                        OrdersItem itema=new OrdersItem(productorderno,productorderdate,productimgurl,producturl,
                                productname,productorderprice,productstatus,productstatuscode,producttype,
                                producttypecode,producttransactiontype);
                        myordersList.add(itema);


                    }
                    for (int i = 0; i < contacts2.length(); i++) {
                        JSONObject c = contacts2.getJSONObject(i);
                        String id = c.getString("id");
                        String productorderno= c.getString("productorderno");
                        String productorderdate= c.getString("productorderdate");
                        String productimgurl= c.getString("producturl");
                        String producturl= c.getString("producturl");
                        String productname= c.getString("productname");
                        String productorderprice= c.getString("productprice");
                        String productstatus= c.getString("productstatus");
                        String productstatuscode= c.getString("productstatuscode");
                        String producttype= c.getString("producttype");
                        String producttypecode= c.getString("producttypecode");
                        String producttransactiontype= c.getString("producttransactiontype");



                        OrdersItem itemb=new OrdersItem(productorderno,productorderdate,productimgurl,producturl,
                                productname,productorderprice,productstatus,productstatuscode,producttype,
                                producttypecode,producttransactiontype);
                        mypassbooklist.add(itemb);

                    }



                } catch (final JSONException e) {
                    Log.e("", "Json parsing error: " + e.getMessage());
                    mcontext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mcontext,
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                Log.e("", "Couldn't get json from server.");
                mcontext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mcontext,
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
//            ListAdapter adapter = new SimpleAdapter(mcontext, fashionList,
//                    R.layout.fashion_homelist, new String[]{ "name"},
//                    new int[]{R.id.nnn});

           // adap.notifyDataSetChanged();

//            HashMap<String, String>currentItem=homepageimglist.get(0);


//            Glide
//                    .with(mcontext)
//                    .load(currentItem.get("imageurl").toString())
//                    .into(homepageimg);


            adapter.notifyDataSetChanged();
        }
    }



    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = mcontext.getAssets().open("profilepage.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
