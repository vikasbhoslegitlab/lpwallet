package com.lptechlog.lpwallet.fragments;
import com.lptechlog.lpwallet.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class FourFragment extends Fragment{
    Activity mcontext;
    public FourFragment() {
        // Required empty public constructor
    }
    @SuppressLint("ValidFragment")
    public FourFragment(Activity con) {
        // Required empty public constructor
        mcontext=con;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_four, container, false);
    }

}
