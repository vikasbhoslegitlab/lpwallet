package com.lptechlog.lpwallet.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lptechlog.lpwallet.R;


public class UPIFragment extends Fragment{
    Activity mcontext;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    LinearLayout upilayout,banklayout;
    TextView selectfrmphonebook;
    static final int PICK_CONTACT=1;
    EditText phnum;
    String cNumber;
    public UPIFragment() {
        // Required empty public constructor
    }
    @SuppressLint("ValidFragment")
    public UPIFragment(Activity con) {
        // Required empty public constructor
        mcontext=con;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_upi, container, false);
        radioGroup=(RadioGroup)v.findViewById(R.id.radioGroup);
        upilayout=v.findViewById(R.id.upiidlayout);
        banklayout=v.findViewById(R.id.tobanklayout);
        phnum=v.findViewById(R.id.upiphonenumber);
        selectfrmphonebook=v.findViewById(R.id.selfrmphonebook);
        setlistners(v);


        return v;
    }

    private void setlistners(View v) {

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                int index = group.indexOfChild(mcontext.findViewById(checkedId));
                //Toast.makeText(mcontext, ""+index, Toast.LENGTH_SHORT).show();

                switch (index){
                    case 0:
                        upilayout.setVisibility(View.VISIBLE);
                        banklayout.setVisibility(View.GONE);
                        break;
                    case 1:
                        upilayout.setVisibility(View.GONE);
                        banklayout.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        selectfrmphonebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {

                    Uri contactData = data.getData();
                    Cursor c = mcontext.managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {


                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = mcontext.getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            cNumber = phones.getString(phones.getColumnIndex("data1"));
                            System.out.println("number is:" + cNumber);

                            phnum.setText(""+cNumber);

                        }
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        //Toast.makeText(mcontext, ""+cNumber+" selected", Toast.LENGTH_SHORT).show();


                    }
                }
                break;
        }
    }
}
