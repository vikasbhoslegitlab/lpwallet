package com.lptechlog.lpwallet;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class BillPayments extends AppCompatActivity {
    String[] operator = {"Select Operator","Ooredoo", "Vodafone","Abcdef","Ghijkl","Mnopqr"};
    String[] state = {"Select State","Andhra Pradesh", "Assam", "Karnataka", "Maharashtra", "West Bengal", "Punjab", "Haryana", "Uttar Pradesh"};
    String []stateboard={"Select State Board","Abcd","Efgh","Ijkl","Mnop","Qrst"};

    String[] operator_dth = {"Select Operator","Ac", "Cd", "Ef", "Gh", "Ij", "Kl", "Mn", "Op"};

    AutoCompleteTextView actv;
    Intent in;
    int key;
    LinearLayout layout_recharge,layout_electricity,layout_dth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_payments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setids();
        setallidsgone();
        in=getIntent();

        key=in.getIntExtra("key",0);

        setoneidvisible(key);


    }

    private void setoneidvisible(int key) {

        switch (key){

            case 0:
                { layout_recharge.setVisibility(View.VISIBLE);
                    setTitle("Recharge");
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (this, android.R.layout.select_dialog_item, operator);
                //Getting the instance of AutoCompleteTextView
                AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.recahrgeoperator);
                actv.setThreshold(1);//will start working from first character
                actv.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                actv.setTextColor(Color.RED);
                    break;
                 }
            case 1: {
                layout_electricity.setVisibility(View.VISIBLE);
                ArrayAdapter<String> adapter_electricity = new ArrayAdapter<String>
                        (this, android.R.layout.select_dialog_item, state);

                ArrayAdapter<String> adapter_stateboard = new ArrayAdapter<String>
                        (this,android.R.layout.simple_spinner_item,stateboard);

                    // Specify the layout to use when the list of choices appears
                adapter_stateboard.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    // Apply the adapter to the spinner

                //Getting the instance of AutoCompleteTextView
                AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.electricitystate);
                EditText et_billnumber=(EditText) findViewById(R.id.electricitybillno);
                EditText electricityamount=(EditText) findViewById(R.id.electricityamount);
                Spinner sp_stateboard=(Spinner)findViewById(R.id.electricitystateboard);
                Button electricity_pay=(Button) findViewById(R.id.electricitypay) ;

                actv.setThreshold(1);//will start working from first character
                actv.setAdapter(adapter_electricity);//setting the adapter data into the AutoCompleteTextView
                actv.setTextColor(Color.RED);

                sp_stateboard.setAdapter(adapter_stateboard);



                setTitle("Electricity Bill");



                break;
            }
            case 2: {
                layout_dth.setVisibility(View.VISIBLE);
                setTitle("DTH Bill");
                ArrayAdapter<String> adapter_operator = new ArrayAdapter<String>
                        (this,android.R.layout.simple_spinner_item,operator_dth);
                adapter_operator.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                Spinner sp_operator=(Spinner)findViewById(R.id.dthoperatorspinner);
                EditText dth_billnumber=(EditText) findViewById(R.id.dthcustomerid);
                EditText dthamount=(EditText) findViewById(R.id.dthamount);
                Button dth_pay=(Button) findViewById(R.id.dthypay) ;

                sp_operator.setAdapter(adapter_operator);

                break;
            }
        }

    }


    private void setids() {
        layout_recharge=findViewById(R.id.rechargelayout);
        layout_electricity=findViewById(R.id.electricitylayout);
        layout_dth=findViewById(R.id.dthlayout);

    }

    private void setallidsgone() {
        layout_recharge.setVisibility(View.GONE);
        layout_electricity.setVisibility(View.GONE);
        layout_dth.setVisibility(View.GONE);
    }

}
