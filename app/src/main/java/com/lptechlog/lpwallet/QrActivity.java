package com.lptechlog.lpwallet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.lptechlog.lpwallet.custom.CustomScannerActivity;
import com.lptechlog.lpwallet.custom.SmallCaptureActivity;

public class QrActivity extends Activity {
    Button scan;
    TextView phonenumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        phonenumber=findViewById(R.id.phonenumbercode);
        new IntentIntegrator(QrActivity.this).setOrientationLocked(false).setCaptureActivity(CustomScannerActivity.class).setBeepEnabled(false).initiateScan();

        scan=findViewById(R.id.scan);
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                IntentIntegrator integrator = new IntentIntegrator(QrActivity.this);
//                integrator.setOrientationLocked(false);
//                integrator.setCaptureActivity(SmallCaptureActivity.class);
//                integrator.initiateScan();

                    }
        });

//        IntentIntegrator i=new IntentIntegrator(QrActivity.this);
//        i.setPrompt("Scan QR Code");
//
//        i.setOrientationLocked(true);
//        i.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult ir=IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        
        if(ir!=null){
            if(ir.getContents()==null){
                Toast.makeText(this, "no data found", Toast.LENGTH_SHORT).show();
                try {
                     onBackPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else{
                if(ir.getContents().contains("lpwallet.com")) {
                    Toast.makeText(this, "" + ir.getContents(), Toast.LENGTH_SHORT).show();

                    String string =  ir.getContents();
                    String[] parts = string.split("@");
                    phonenumber.setText(""+parts[0]);

                    Intent i= new Intent(QrActivity.this,Payamount.class);
                    i.putExtra("name","headingyoyolpwallet");
                    i.putExtra("number",""+parts[0]);
                    startActivity(i);
                    finish();

                }
                else{
                    Toast.makeText(this, "wrong data" , Toast.LENGTH_SHORT).show();
                    //new IntentIntegrator(QrActivity.this).setOrientationLocked(false).setCaptureActivity(CustomScannerActivity.class).initiateScan();

                }

            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
           // onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}